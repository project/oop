
OOP
------------------------
This set of modules is intended for developers. It does not provide functionality to end-users. It only provides a mechanism to help developers with code organization.

To install, place the entire oop folder into your modules directory.
Go to administer -> site building -> modules and enable the OOP module and any desired related modules.

Go to administer -> site configuration -> OOP settings to configure information about class files and class names.


Example
------------------------
Note: This example doesn't work if you have the devel module enabled, because it loads user.module prematurely (during bootstrap). To try this out, temporarily disable the devel module. You can apply the concept of this example in extending modules that aren't loaded during Drupal's bootstrap phase.

If you have oop.module and oop_module.module enabled, "OrganizationX_" as your value for "Namespaces", and "classes" as your value for "Autoload -> Classes directory", and defaults for your other settings, then if you have a class "OrganizationX_Drupal_Modules_User" implemented in a file "classes/OrganizationX/Drupal/Modules/User.php" within the directory where your "settings.php" file is, and that class has the following implementation:

<code>
  class OrganizationX_Drupal_Modules_User extends Drupal_Module {
    
    /**
     * interfaceVersion
     * Update this (increment the number) whenever a function in this class is added, removed, or its signature modified.
     */
    function interfaceVersion() {
      return '0.' . parent::interfaceVersion();
    }
    
    /**
     * Implementation of hook_block
     */
    function block($op = 'list', $delta = 0, $edit = array()) {
      global $user;
      
      // Default implementation in user.module prevents the user block from appearing
      // on the registration page, but we want to override that.
      if ($op == 'view' && $delta == 0) {
        if (!$user->uid) {
          $block = array(
            'subject' => t('User login'),
            'content' => drupal_get_form('user_login_block'),
          );
          return $block;
        }
      }
      else {
        // On PHP 5.2.6, you can do this
        return parent::block($op, $delta, $edit);
        // Prior to PHP 5.2.6, do this instead
        // return parent::__call('block', array($op, $delta, $edit));
      }
    }
    
  }
</code>

With the above example, this class will be instantiated and its "block" method will be called whenever the "user_block" function is normally called, allowing you to alter user.module's behavior without modifying user.module itself.
